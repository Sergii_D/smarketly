app.controller('testController', function($location, $scope, $rootScope, validationService, cardFactory) {
    $scope.newUser = {};
	$scope.formData = {};
	$scope.cardlist = cardFactory.getCards();

    var namePattern = /^[a-zA-Z0-9]{2,30}$/,
        emailPattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/,
        passwordPattern = /^[a-zA-Z0-9_.-]{6,15}$/;

    $scope.clearValidationObj = function(){
        $scope.validObj = {
            "nameError": false,
            "emailError": false,
            "passwordError": false,
            "confPasswordError": false
        }
    };
    $scope.clearValidationObj();
    

        $scope.submitNewUser = function(){          
            !validationService.isValid($scope.newUser.userName, namePattern)? $scope.validObj.nameError = true : $scope.validObj.nameError = false;
            !validationService.isValid($scope.newUser.userEmail, emailPattern)? $scope.validObj.emailError = true : $scope.validObj.emailError = false;
            !validationService.isValid($scope.newUser.userPass, passwordPattern)? $scope.validObj.passwordError = true : $scope.validObj.passwordError = false;
            $scope.newUser.userPass != $scope.newUser.confirmPass ? $scope.validObj.confPasswordError = true : $scope.validObj.confPasswordError = false;

            if(!$scope.validObj.emailError && !$scope.validObj.passwordError && !$scope.validObj.nameError &&!$scope.validObj.confPasswordError){
                $rootScope.users.push($scope.newUser);
                alert("new user is created");
                $scope.newUser = {};
                $scope.clearValidationObj();
                $location.path("/login");
            }                
      };

		$scope.submitContact = function(){          
            
            !validationService.isValid($scope.formData.contactEmail, emailPattern)? $scope.validObj.emailError = true : $scope.validObj.emailError = false;
            !validationService.isValid($scope.formData.contactPassword,passwordPattern)? $scope.validObj.passwordError = true : $scope.validObj.passwordError = false;

            if(!$scope.validObj.emailError && !$scope.validObj.passwordError){
            	var data = {
                        email: $scope.formData.contactEmail,
                        password: $scope.formData.contactPassword
                };
                console.log(data);
                if($rootScope.users.length ){
                    for(var i = 0; i< $rootScope.users.length; i++){
                        if($rootScope.users[i].userEmail==$scope.formData.contactEmail && $rootScope.users[i].userPass==$scope.formData.contactPassword){
                            $scope.user = $rootScope.users[i].userName;
                            $scope.formData = {};
                            $scope.clearValidationObj();
                            return;
                        } 
                    }
                } else {
                    alert("No user with such login and email");
                }
               
            }
                     
      };

      $scope.logOut = function(){
        $scope.user = '';
      };
     

});