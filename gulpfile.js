'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');
    

gulp.task('sass', function () {
  return gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function () {
   var files = [
      './*.html',
      './css/*.css',
      './img/*.{png,jpg,gif}',
      './js/*.js'
   ];

   browserSync.init(files, {
      server: {
         baseDir: "./"
      }
   });

});
gulp.task('default', ['browser-sync'], function() {
    gulp.start('sass:watch');
});

gulp.task('clean', function() {
    return del(['dist']);
});
gulp.task('usemin', function() {
  return gulp.src('./*.html')
        .pipe(usemin({
            css: [  rev ],
            inlinecss: [ cleanCss, 'concat' ],
            js: [ rev ],
            inlinejs: [ uglify ],
            html: [ function() { return htmlmin({ collapseWhitespace: true })} ]
        }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('build', ['clean'], function(){
  gulp.start('usemin');
});